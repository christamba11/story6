# Story 6 and Above, PPW
## Made by
- Christopher Y Hasian Tamba
- 1806235681
- PPW B

## Link Herokuapp Story 6
```go
https://story6-christo.herokuapp.com/
```
## Link Herokuapp Story 7
```go
https://story6-christo.herokuapp.com/story7
```
## Link Herokuapp Story 8
```go
https://story6-christo.herokuapp.com/story8
```

## Link Herokuapp Story 9
```go
https://story6-christo.herokuapp.com/story9
```
### Notes for Story 9
# For this story you can try login with username: christo and password: test12345
# If you want to create a new user, you can go to ```go https://story6-christo.herokuapp.com/admin``` and login with username: admin and password: admin

