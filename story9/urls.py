from django.contrib import admin
from django.urls import path, include
from . import views

app_name = "story9"

urlpatterns = [
    path('', views.home_view, name="home"),
    path('login/', views.login_view, name="login"),
    path('logout/', views.logout_view, name="logout") 
]
