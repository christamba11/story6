from django.test import TestCase, Client
from django.urls import resolve
from .views import login_view

class Story9PageUnitTest(TestCase):
    def test_story9_home_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code,302)
        
    def test_story9_login_url_is_exist(self):
        response = Client().get('/story9/login')
        self.assertEqual(response.status_code, 301)
    
    def test_story9_login_using_right_function(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, login_view)

    def test_story9_login_using_right_template(self):
        response = self.client.get('/story9/login/')
        self.assertTemplateUsed(response, 'story9/login_view.html')
