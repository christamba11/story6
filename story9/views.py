from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout

def home_view(request):
    user = request.user
    if user.is_authenticated:
        return render(request, "story9/home.html")
    else:
        return redirect("story9:login")

def login_view(request):
    user = request.user
    login_form = AuthenticationForm()

    if user.is_authenticated:
        return render(request,'story9/home.html')
    if request.method == "POST":
        login_form = AuthenticationForm(data = request.POST)
        if login_form.is_valid():
            user = login_form.get_user()
            login(request, user)
            return redirect("story9:home")
    return render(request,'story9/login_view.html', {'form':login_form})

def logout_view(request):
    if request.method == "POST":
        logout(request)
    return redirect('story9:login')