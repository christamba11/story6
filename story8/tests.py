from django.test import TestCase, Client
from django.urls import resolve
from .views import find_books

class Story8PageUnitTest(TestCase):
    def test_story8_home_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_story8_home_using_right_function(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, find_books)

    def test_story8_home_using_right_template(self):
        response = self.client.get('/story8/')
        self.assertTemplateUsed(response, 'story8/find_books.html')