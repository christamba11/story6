from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse

def find_books(request):
    return render(request, "story8/find_books.html")
