from django.contrib import admin
from django.urls import path, include
from . import views

app_name = "story8"

urlpatterns = [
    path('', views.find_books, name="find_books"), 
]
