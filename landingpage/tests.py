from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from .views import add_status
from .models import Status
from .forms import AddStatusForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options 

#story6 functional test
class Story6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options = chrome_options)
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        #Opening the link we want to test
        selenium.get('http://localhost:8000')

        #find the form element
        title = selenium.find_element_by_id('id-title')
        submit = selenium.find_element_by_id('submit')

        #Fill the form with data as written in story
        title.send_keys("Coba Coba")

        #submitting the form
        submit.send_keys(Keys.RETURN)

        #cek apakah ada "Coba Coba" ada
        self.assertIn("Coba Coba", self.selenium.page_source)


class LandingPageUnitTest(TestCase):
    def test_landingpage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_landingpage_using_right_function(self):
        found = resolve('/')
        self.assertEqual(found.func, add_status)

    def test_model_can_add_new_status(self):
        #Adding a status
        new_status = Status.objects.create(title="Sedang sibuk")
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)

        all_obj = Status.objects.all()
        the_obj = all_obj[0]

        self.assertEqual(the_obj.title, "Sedang sibuk")
        self.assertEqual(the_obj.date_and_time.hour, timezone.now().hour)
        self.assertEqual(the_obj.date_and_time.minute, timezone.now().minute)
        self.assertEqual(the_obj.date_and_time.date(), timezone.now().date())

    #test for getting the model object
    def test_get_model(self):
        add_status = Status.objects.create(title="Main")
        get_status = Status.objects.get(title="Main")
        self.assertEqual(add_status, get_status)

    def test_form_has_placeholder_and_css_class(self):
        form = AddStatusForm()
        self.assertIn('class="form-body"', form.as_p())
        self.assertIn('placeholder', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = AddStatusForm(data={'title':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['title'], ['This field is required.']
        )

    #test if the form is rendered in the page
    def test_page_contained_form(self):
        response = Client().get('/')
        self.assertContains(response, AddStatusForm()['title'])

    def test_add_status_using_the_right_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingpage/home.html')

    def test_post_success_and_render_the_result(self):
        user_input = "Test input"
        response_post = Client().post('/', {'title':user_input})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(user_input, html_response)
        self.assertIn("Halo, apa kabar?", html_response)



    
    