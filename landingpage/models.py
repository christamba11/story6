from django.db import models
from django.utils import timezone

class Status(models.Model):
    title = models.CharField(max_length=300)
    date_and_time = models.DateTimeField(auto_now_add = True)
