from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Status
from .forms import AddStatusForm

def add_status(request):
    form = AddStatusForm()
    list_status = Status.objects.all().order_by('date_and_time')
    if request.method == "POST":
        form = AddStatusForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    return render(request, "landingpage/home.html", {"form":form, "list_status":list_status})
