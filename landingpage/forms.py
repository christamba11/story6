from django import forms
from .models import Status

class AddStatusForm(forms.ModelForm):
    title = forms.CharField(
        widget = forms.Textarea(
            attrs = {
                'class':'form-body',
                'placeholder' : "Enter a new status",
                'required' : True,
                'id' :'id-title',
            }
        )
    )
    class Meta:
        model = Status
        fields = [
            'title',
        ]