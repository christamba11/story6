from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from . import views
from django.http import HttpRequest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

"""Unit test for story7 app"""
class Story7UnitTest(TestCase):
    def test_story7_url_is_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_story7_home_using_right_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, views.profile_views)

    def test_story7_home_using_right_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/story7-home.html')

    def test_story7_template_contains_section_required(self):
        request = HttpRequest()
        response = views.profile_views(request)
        self.assertContains(response, "My Activities")
        self.assertContains(response, "My Experiences")
        self.assertContains(response, "My Achievements")


    


