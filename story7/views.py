from django.shortcuts import render
from django.http import HttpResponse

def profile_views(request):
    return render(request, "story7/story7-home.html")
