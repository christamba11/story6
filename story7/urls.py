from django.contrib import admin
from django.urls import path, include
from . import views

app_name = "story7"

urlpatterns = [
    path('', views.profile_views , name = "profile_views"),
]
