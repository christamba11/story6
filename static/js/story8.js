const formatBook = (books) => {
    $('#results').append(    
        "<tr>" +
            "<th class='subtitle table-title'>Cover</th>" +
            "<th class='subtitle table-title'>Title</th>" +
            "<th class='subtitle table-title'>Author(s)</th>" +
            "<th class='subtitle book-desc table-title'>Description</th>" +
        "</tr>"
    );
    for(i=0 ; i<books.items.length; i++){
        $('#results').append("<tr>");
        try{    
            $('#results').append("<td><a target='_blank' href='"+ books.items[i].volumeInfo.previewLink +"'> <img src='" + books.items[i].volumeInfo.imageLinks.thumbnail + "'></img></a></td>");
        }
        catch{
            $("#results").append("<td>No image</td>");                   
        }
        $('#results').append("<td><a target='_blank' href='"+ books.items[i].volumeInfo.infoLink +"'>" + books.items[i].volumeInfo.title + "</a></td>");                    
        try{
            if(books.items[i].volumeInfo.authors.length > 0){
                $("#results").append(`
                <td>
                    <ul>
                        ${books.items[i].volumeInfo.authors.map((obj) => {
                            return `<li>${obj}</li>`
                        }).join("")}
                    </ul>
                </td>
                `)
            }
            else{
                $("#results").append("<td>Unknown</td>");
            }
        }
        catch{
            $("#results").append("<td>Unknown</td>");  
        }
        try{
            $('#results').append("<td class='book-desc'>"+books.items[i].searchInfo.textSnippet+"</td>");  
        }
        catch{
            $("#results").append("<td class='book-desc'>No details</td>");  
        }
        $('#results').append("</tr>");
    }
}


$(document).ready(() => {
    $('#results')[0].innerHTML = "<h3> Loading page... </h3>";
    

    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=web-programming',
        success: function(books){
            $('#results').empty();
            formatBook(books)
        }
    });

    $('#btn-search').on('click', function(){
        let key = $("#search").val();
        $('#results')[0].innerHTML = "<h3> Searching... </h3>"

        $.ajax({
            method: 'GET',
            url: 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function(books){
                $('#results').empty();
                formatBook(books)

            }
        })
    });

    $('#search').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            let key =event.target.value;
            $('#results')[0].innerHTML = "<h3> Searching... </h3>"

            $.ajax({
                method: 'GET',
                url: 'https://www.googleapis.com/books/v1/volumes?q=' + key,
                success: function(books){
                    $('#results').empty();
                    formatBook(books)
    
                }
            })
        }
    });

})
