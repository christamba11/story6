from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse

def story10_home(request):
    response = {'port':request.META['SERVER_PORT']}
    return render(request, "story10/story10.html", response)
