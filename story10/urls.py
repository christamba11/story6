from django.contrib import admin
from django.urls import path, include
from . import views

app_name = "story10"

urlpatterns = [
    path('', views.story10_home, name="home"),
]